; Makefile for distribution with Drupal core and "baseline" profile
;
; Command: drush make build-baseline.make <target directory>

api = 2
core = 7.x

projects[] = drupal 

; Add Student CRM to the full distribution build.
projects[baseline][type] = profile
; projects[baseline][version] = 1.x-dev
; projects[baseline][download][type] = git
projects[baseline][download][url] = https://github.com/stevenkkim/drupal-baseline.git
projects[baseline][download][branch] = master
